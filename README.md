# Front-end site Posto do Sertão

[ ![Codeship Status for raisiqueira/postodosertao](https://app.codeship.com/projects/9a856020-f11e-0134-a265-7e8cdab40218/status?branch=master)](https://app.codeship.com/projects/209284)

Libs usadas:

| Lib     | Link                                            |
|------------------------|----------------------------------|
| GULPJS       | https://www.npmjs.com/package/gulp         |
| CSSNano      | https://www.npmjs.com/package/gulp-cssnano |
| HTML Min     | https://www.npmjs.com/package/gulp-htmlmin |
| IMG Min      | https://www.npmjs.com/package/gulp-imagemin|
| Gulp SASS    | https://www.npmjs.com/package/gulp-sass    |
| Browser Sync | https://www.npmjs.com/package/browser-sync |
|AOS           | http://michalsnik.github.io/aos/           |


| 360 Gallery | Lib                                         |
--------------|---------------------------------------------|
| A-frame     | https://github.com/aframevr/aframe/tree/v0.5.0/examples/boilerplate/panorama |
| A-frame React | https://github.com/ngokevin/aframe-react |

| Experimental | Link                                       |
|--------------|--------------------------------------------|
| React VR     |https://facebook.github.io/react-vr/        |

Frameworks Front-end usados:

- Bootstrap _testes_
- Jquery
- [Sierra SCSS](https://sierra-library.github.io/)

Em Construção :construction:

## Install & Build
`npm install && bower install`

`gulp serve` para ver as mudanças no código

`gulp serve:dist` para ver um pré-view da produção

`gulp` para Produção

- Gerado com [Yeoman](https://github.com/yeoman/generator-webapp)