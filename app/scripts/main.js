console.log('\'Allo \'Allo!');

// AOS Init
AOS.init();

// Parallax Header
var $parallaxElement  = $('.parallax-bg');
var elementHeight     = $parallaxElement.outerHeight();

function parallax() {

  var scrollPos = $(window).scrollTop();
  var transformValue = scrollPos/40;
  var opacityValue =  1 - ( scrollPos / 2000);
  var blurValue = Math.min(scrollPos / 100, 3);

  if ( scrollPos < elementHeight ) {
    $parallaxElement.css({
      'transform': 'translate3d(0, -' + transformValue + '%, 0)',
      'opacity': opacityValue,
      '-webkit-filter' : 'blur('+blurValue+'px)'
    });
  }
}

$(window).scroll(function() {
  parallax();
});
// Headroom.js
var header = document.getElementById('header');
var headroom  = new Headroom(header);
headroom.init();


// Swiper
var mySwiper = new Swiper ('.swiper-container', {
    // Optional parameters
    direction: 'horizontal',
    loop: false,

    // If we need pagination
    pagination: '.swiper-pagination',

    // Navigation arrows
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev',
    slidesPerView: 3,
    paginationClickable: true,
    spaceBetween: 30
  });